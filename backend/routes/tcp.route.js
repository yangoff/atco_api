const Tcp = require("../models/tcp");
const express = require("express");

const app = express();

app.post("/api/tcps", (req, res, next) => {
  const tcp = new Tcp({
    nometcp: req.body.nometcp,
    regtcp: req.body.regtcp,
    niveltcp: req.body.niveltcp
  });
  tcp.save().then(createdTcp => {
    res.status(201).json({
      message: "Tcp added successfully",
      tcpId: createdTcp._id
    });
  });
});

app.put("/api/tcps/:id", (req, res, next) => {
  const tcp = new Tcp({
    _id: req.body.id,
    nometcp: req.body.nometcp,
    regtcp: req.body.regtcp,
    niveltcp: req.body.niveltcp
  });
  Tcp.updateOne({ _id: req.params.id }, tcp).then(result => {
    res.status(200).json({ message: "Update successful!" });
  });
});

app.get("/api/tcps", (req, res, next) => {
  Tcp.find().then(documents => {
    res.status(200).json({
      message: "Tcps fetched successfully!",
      tcps: documents
    });
  });
});

app.delete("/api/tcps/:id", (req, res, next) => {
  Tcp.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({ message: "Tcp deleted!" });
  });
});

// app.get("/api/tcps/:id",(req,res,next)=>{
// Tcp.findById({_id: req.params.id}).then(documents=>{
//   res.status(200).json({
//     tcps: documents
//   })
// })
// })

app.get("/api/tcps/:id", (req, res, next) => {
  Tcp.findById(req.params.id).then(tcp => {
    if (tcp) {
      res.status(200).json(tcp);
    } else {
      res.status(404).json({ message: "Tcp not found!" });
    }
  });
});

module.exports = app;
