const mongoose = require('mongoose');

const tcpSchema = mongoose.Schema({
  nometcp: { type: String, required: true },
  regtcp: { type: Number, required: true },
  niveltcp: {type:Number,require:true}
});

module.exports = mongoose.model('Tcp', tcpSchema);
